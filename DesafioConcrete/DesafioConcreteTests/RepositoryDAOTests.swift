//
//  RepositoryDAOTests.swift
//  DesafioConcrete
//
//  Created by Lázaro on 01/12/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import XCTest

@testable import DesafioConcrete

class RepositoryDAOTests: XCTestCase {
    
    func testGetAllRepositorysWith() {
        
        var repositories: [Repository] = []
        var gitError = GitError()
        
        let asyncExpectation = expectation(description: "Request return")
        
        let completionBlock = { (result: [Repository], error: GitError?) in
            repositories = result
            
            if let err = error {
                gitError = err
            }
            
            asyncExpectation.fulfill()
            
        }
        
        RepositoryDAO.getAllRepositorysWith(page: "3", completionHandler: completionBlock)
        
        waitForExpectations(timeout: 10.0) { (error) in
            XCTAssert(repositories.count > 0)
            XCTAssert(gitError.generalError == nil)
        }
    }
    
    func testFailToGetAllRepositorysWith() {
        
        var repositories: [Repository] = []
        var gitError = GitError()
        
        let asyncExpectation = expectation(description: "Request return")
        
        let completionBlock = { (result: [Repository], error: GitError?) in
            repositories = result
            
            if let err = error {
                gitError = err
            }
            
            asyncExpectation.fulfill()
            
        }
        
        RepositoryDAO.getAllRepositorysWith(page: "1000", completionHandler: completionBlock)
        
        waitForExpectations(timeout: 10.0) { (error) in
            XCTAssert(repositories.count == 0)
            XCTAssert(gitError.generalError == nil)
        }
    }
    
    
    
    
}
