//
//  PullRequestDAOTests.swift
//  DesafioConcrete
//
//  Created by Lázaro on 01/12/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import XCTest

@testable import DesafioConcrete

class PullRequestDAOTests: XCTestCase {
    
    func testGetAllPullRequestWith() {
        
        var pullRequests: [PullRequest] = []
        var gitError = GitError()
        
        let asyncExpectation = expectation(description: "Request return")
        
        let completionBlock = { (result: [PullRequest], error: GitError?) in
            
            pullRequests = result
            
            if let err = error {
                gitError = err
            }
            
            asyncExpectation.fulfill()
            
        }
        
        
        PullRequestDAO.getAllPullRequestWith(login: "ReactiveX", name: "RxJava", completionHandler: completionBlock)
        
        waitForExpectations(timeout: 10.0) { (error) in
            XCTAssert(pullRequests.count > 0)
            XCTAssert(gitError.generalError == nil)
        }
    }
    
    func testFailGetAllPullRequestWith(){
        
        var pullRequests: [PullRequest] = []
        var gitError = GitError()
        
        let asyncExpectation = expectation(description: "Request return")
        
        let completionBlock = { (result: [PullRequest], error: GitError?) in
            
            pullRequests = result
            
            if let err = error {
                gitError = err
            }
            
            asyncExpectation.fulfill()
            
        }
        
        PullRequestDAO.getAllPullRequestWith(login: "inexistente", name: "inexistente", completionHandler: completionBlock)
        
        waitForExpectations(timeout: 10.0) { (error) in
            XCTAssert(pullRequests.count == 0)
            XCTAssert(gitError.generalError == nil)
        }
        
        
    }
    
    
    
}
