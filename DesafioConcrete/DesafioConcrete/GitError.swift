//
//  GitError.swift
//  DesafioConcrete
//
//  Created by Lázaro on 01/12/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import UIKit
import ObjectMapper
class GitError: NSObject, Mappable {
    
    var generalError : String?
    var httpCode : Int?
    
    override init() {
        
    }
    
    init(generalError : String, httpCode : Int) {
        
        self.generalError = generalError
        self.httpCode = httpCode
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        generalError    <- map["generalError"]
    }
}

