//
//  RepositoryTableViewCell.swift
//  DesafioConcrete
//
//  Created by Lázaro on 30/11/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import UIKit
import WebImage

class RepositoryTableViewCell: UITableViewCell {
    
    static var cellIdentifier = "RepositoryCell"
    @IBOutlet weak var backgroundShape: UIView!
    @IBOutlet weak var avatarUser: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var forksQuantity: UILabel!
    @IBOutlet weak var starsQuantity: UILabel!
    @IBOutlet weak var repositoryName: UILabel!
    @IBOutlet weak var repositoryDescription: UILabel!

    var repositoryData : Repository! {
        didSet {
            self.setupCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundShape.roundCorner(value: 5)
    }
    
    func setupCell() {
        self.avatarUser.sd_setImage(with: URL(string: repositoryData.owner.avatarUrl!), placeholderImage: UIImage(named: "user.png"))
        self.avatarUser.roundToCircle()
        self.username.text = repositoryData.owner.login
        self.name.text = repositoryData.userName
        self.forksQuantity.text = repositoryData.forksQuantity.description
        self.starsQuantity.text = repositoryData.starsQuantity.description
        self.repositoryName.text = repositoryData.name
        self.repositoryDescription.text = repositoryData.repositoryDescription
        
    }

}
