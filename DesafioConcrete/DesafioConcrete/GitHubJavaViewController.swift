//
//  ViewController.swift
//  DesafioConcrete
//
//  Created by Lázaro on 30/11/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

private let segueDetailRepository = "DetailRepositorySegue"
class GitHubJavaViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var repositoryList: [Repository] = []
    var loadingBallSpin: NVActivityIndicatorView!
    var loadingBallClip: NVActivityIndicatorView!
    var currentPage = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLoading()
        setupTableView()
        getRepositorysWith(page: currentPage)
    }
    
    func setupTableView() {
        let nib = UINib(nibName: RepositoryTableViewCell.cellIdentifier, bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: RepositoryTableViewCell.cellIdentifier)
    }
    
    func getRepositorysWith(page: Int) {
        RepositoryDAO.getAllRepositorysWith(page: "\(page)"){ (result: [Repository], error: GitError?) in
            
            if let unwrappedError = error {
                AlertUtils.showAlertError(error: unwrappedError, viewController: self)
            } else {
                for repository in result {
                    self.repositoryList.append(repository)
                }
                self.tableView.reloadData()
                self.loadingBallSpin.stopAnimating()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == segueDetailRepository {
            if let indexPath = tableView.indexPathForSelectedRow{
                let destinationViewController = segue.destination as! RepositoryDetailViewController
                destinationViewController.repository = self.repositoryList[indexPath.row]
            }
        }
    }
    
}

extension GitHubJavaViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return repositoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RepositoryTableViewCell.cellIdentifier, for: indexPath) as! RepositoryTableViewCell
        cell.repositoryData = self.repositoryList[indexPath.row]
        return cell
    }
    
}

extension GitHubJavaViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: segueDetailRepository, sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastRowIndex = tableView.numberOfRows(inSection: 0)
        if (indexPath.row == (lastRowIndex - 1)) {
            loadingBallSpin.startAnimating()
            self.currentPage += 1
            getRepositorysWith(page: currentPage)
        }
    }
}

extension GitHubJavaViewController: NVActivityIndicatorViewable {

    func setupLoading() {
        self.loadingBallSpin = NVActivityIndicatorView(frame: CGRect(x: self.view.center.x - 25, y: self.view.center.y, width: 50, height: 50) , type: .ballSpinFadeLoader, color: UIColor.white, padding: CGFloat(0))
        self.view.addSubview(loadingBallSpin)
        loadingBallSpin.startAnimating()
        
    }
}
