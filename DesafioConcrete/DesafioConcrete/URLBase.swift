//
//  URLBase.swift
//  DesafioConcrete
//
//  Created by Lázaro on 30/11/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import UIKit

/** Enum para selecionar qual a url base para o endpoint
 * localhost : http://localhost:3000
 * teste : http://testeapp.com
 * production : http://supervisor.com
 */
enum URLBase {
    
    case localhost
    case test
    case production
    
    private func url() -> String {
        switch self {
        case .localhost:
            return "http://localhost:3000"
        case .test:
            return ""
        case .production:
            return "https://api.github.com"
        }
    }
    
    private func toCode() -> Int {
        switch self {
        case .localhost:
            return 0
        case .test:
            return 1
        case .production:
            return 2
        }
    }
    
    private static func fromCode(_ code : Int) -> URLBase {
        switch code {
        case 0:
            return .localhost
        case 1:
            return .test
        case 2:
            return .production
        default:
            return .production
        }
    }
    
    private static func getMode() -> URLBase {
        
        let userDefaults : UserDefaults = UserDefaults.standard
        
        if let urlCode : Int = userDefaults.object(forKey: "URLBase") as? Int {
            return fromCode(urlCode)
        }else{
            return .production
        }
    }
    /** Esta função define a urlBase de todo o Projeto, ela deve ser chamada no didFinishLaunch na AppDelegate
     
     */
    public static func initURLMode(mode : URLBase){
        
        let userDefaults : UserDefaults = UserDefaults.standard
        userDefaults.set(mode.toCode(), forKey: "URLBase")
        userDefaults.synchronize()
    }
    
    /** Função para restornar a urlBase com o endpoint
     
     */
    static func endpoint(_ endpoint: Endpoint) -> String{
        return getMode().url() + endpoint.description()
    }
}
