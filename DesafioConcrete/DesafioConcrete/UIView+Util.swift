//
//  UIView+Util.swift
//  DesafioConcrete
//
//  Created by Lázaro on 30/11/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import UIKit

extension UIView {
        
    //MARK: Arredondar bordas de uma view
    
    func roundCorner(value : CGFloat){
        self.layer.cornerRadius = value
        self.layer.masksToBounds = true
    }

}
