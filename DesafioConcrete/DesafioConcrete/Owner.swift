//
//  Owner.swift
//  DesafioConcrete
//
//  Created by Lázaro on 30/11/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import UIKit
import ObjectMapper
class Owner: NSObject, Mappable {
    
    var login: String!
    var avatarUrl: String!
    
    func mapping(map: Map) {
        login <- map["login"]
        avatarUrl <- map["avatar_url"]
    }
    
    override init() {
        super.init()
        self.login = ""
        self.avatarUrl = ""        
    }
    
    init(login: String, avatarUrl: String) {
        self.login = login
        self.avatarUrl = avatarUrl
    }
    
    required init?(map: Map) {
        
    }
    
}
