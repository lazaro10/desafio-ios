//
//  Repository.swift
//  DesafioConcrete
//
//  Created by Lázaro on 30/11/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import UIKit
import ObjectMapper

class Repository: NSObject, Mappable {
    
    var owner: Owner!
    var name: String!
    var repositoryDescription: String?
    var userName: String!
    var forksQuantity: Int!
    var starsQuantity: Int!
    
    override init() {
        super.init()
        
        self.name = ""
        self.repositoryDescription = ""
        self.owner = Owner()
        self.userName = ""
        self.forksQuantity = 0
        self.starsQuantity = 0
    }
    
    init(name: String, repositoryDescription: String, userName: String, forksQuantity: Int, starsQuantity: Int, owner: Owner) {
        self.name = name
        self.repositoryDescription = repositoryDescription
        self.owner = owner
        self.userName = userName
        self.forksQuantity = forksQuantity
        self.starsQuantity = starsQuantity
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        repositoryDescription <- map["description"]
        userName <- map["full_name"]
        forksQuantity <- map["forks_count"]
        starsQuantity <- map["stargazers_count"]
        owner <- map["owner"]
    }
    

}
