//
//  PullRequest.swift
//  DesafioConcrete
//
//  Created by Lázaro on 30/11/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import UIKit
import ObjectMapper

class PullRequest: NSObject, Mappable {
    
    var title: String!
    var body: String!
    var createdAt: String!
    var owner: Owner!
    var htmlUrl: String!
    
    func mapping(map: Map) {
        title <- map["title"]
        body <- map["body"]
        createdAt <- map["created_at"]
        owner <- map["user"]
        htmlUrl <- map["html_url"]
    }
    
    override init() {
        super.init()
        self.title = ""
        self.body = ""
        self.createdAt = ""
        self.owner = Owner()
        self.htmlUrl = ""
        
    }
    
    init(title: String, body: String, createdAt: String, owner: Owner, htmlUrl: String ) {
        self.title = title
        self.body = body
        self.createdAt = createdAt
        self.owner = owner
        self.htmlUrl = htmlUrl
    }
    
    required init?(map: Map) {
        
    }
}
