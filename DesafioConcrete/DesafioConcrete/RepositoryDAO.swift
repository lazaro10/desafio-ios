//
//  RepositoryDAO.swift
//  DesafioConcrete
//
//  Created by Lázaro on 01/12/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class RepositoryDAO: NSObject {
    
    /** Função para buscar todos os Repositórios
     */
    static func getAllRepositorysWith(page: String, completionHandler completion :@escaping (_ groups : [Repository], _ error : GitError?) -> Void){
    
        let url = URLBase.endpoint(Endpoint.repository)+"?q=language:Java&sort=stars&page="+page

        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding(), headers: nil).responseJSON { (response) in
            
            if response.result.isFailure {
                
                if let httpStatusCode = response.response?.statusCode {
                    completion([Repository](), ErrorUtils.errorWithStatusCode(httpStatusCode: httpStatusCode))
                    
                }else{
                    completion([Repository](), ErrorUtils.connectionFailed())
                }
            }else{
                if let resultJSON = response.result.value {
                    
                    if response.response?.statusCode == 200 {
                        
                        
                        if let jsonResult = response.result.value as? [String: Any] {
                            if let items = jsonResult["items"] as? [[String: Any]] {
                                if let response = Mapper<Repository>().mapArray(JSONObject: items){
                                    completion(response, nil)
                                }
                            }
                        }else{
                            
                            completion([Repository](), ErrorUtils.dontHaveData())
                        }
                        
                    }else{
                        
                        let error = Mapper<GitError>().map(JSONObject: resultJSON)
                        error?.httpCode = response.response?.statusCode
                        
                        completion([Repository](), error)
                    }
                }else {
                    
                    completion([Repository](), ErrorUtils.errorWithStatusCode(httpStatusCode: (response.response?.statusCode)!))
                }
            }
        }
    }

}
