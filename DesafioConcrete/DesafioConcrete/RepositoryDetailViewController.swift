//
//  RepositoryDetailViewController.swift
//  DesafioConcrete
//
//  Created by Lázaro on 30/11/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class RepositoryDetailViewController: UIViewController  {

    @IBOutlet weak var tableView: UITableView!
    var repository: Repository = Repository()
    var pullRequestList: [PullRequest] = []
    var loadingBallTriangle: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLoading()
        setupTableView()
        getRepositorys()
        self.title = self.repository.name
    }
    
    func setupTableView() {
        let nib = UINib(nibName: PullRequestTableViewCell.cellIdentifier, bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: PullRequestTableViewCell.cellIdentifier)
    }
    
    func getRepositorys() {
        PullRequestDAO.getAllPullRequestWith(login: repository.owner.login, name: repository.name){ (result: [PullRequest], error: GitError?) in
            
            if let unwrappedError = error {
                
                AlertUtils.showAlertError(error: unwrappedError, viewController: self)
                
            } else {
                self.loadingBallTriangle.stopAnimating()
                self.pullRequestList = result
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func popView(_ sender: Any) {
         _ = self.navigationController?.popViewController(animated: true)
    }
    
}

extension RepositoryDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return pullRequestList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PullRequestTableViewCell.cellIdentifier, for: indexPath) as! PullRequestTableViewCell
        cell.pullRequestData = pullRequestList[indexPath.row]
        return cell
    }
    
}

extension RepositoryDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pullRequest = pullRequestList[indexPath.row]
            UIApplication.shared.openURL(URL(string: pullRequest.htmlUrl)!)
    }
    
}

extension RepositoryDetailViewController: NVActivityIndicatorViewable {

    func setupLoading() {
        self.loadingBallTriangle = NVActivityIndicatorView(frame: CGRect(x: self.view.center.x - 25, y: self.view.center.y, width: 50, height: 50) , type: .ballTrianglePath, color: UIColor.white, padding: CGFloat(0))
        self.view.addSubview(loadingBallTriangle)
        loadingBallTriangle.startAnimating()
    }
    
}
