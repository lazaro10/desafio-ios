//
//  PullRequestDAO.swift
//  DesafioConcrete
//
//  Created by Lázaro on 01/12/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class PullRequestDAO: NSObject {
    
    /** Função para buscar todos os Pull requests
     */
    static func getAllPullRequestWith(login: String, name: String, completionHandler completion :@escaping (_ groups : [PullRequest], _ error : GitError?) -> Void){
        
        let url = URLBase.endpoint(Endpoint.pullRequest)+"/\(login)/\(name)/pulls"
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding(), headers: nil).responseJSON { (response) in
            
            if response.result.isFailure {
                
                if let httpStatusCode = response.response?.statusCode {
                    completion([PullRequest](), ErrorUtils.errorWithStatusCode(httpStatusCode: httpStatusCode))
                    
                }else{
                    completion([PullRequest](), ErrorUtils.connectionFailed())
                }
            }else{
                if let resultJSON = response.result.value {
                    
                    if response.response?.statusCode == 200 {
                        
                        if let response = Mapper<PullRequest>().mapArray(JSONObject: resultJSON){
                            completion(response, nil)
                        }else{
                            
                            completion([PullRequest](), ErrorUtils.dontHaveData())
                        }
                        
                    }else{
                        
                        let error = Mapper<GitError>().map(JSONObject: resultJSON)
                        error?.httpCode = response.response?.statusCode
                        
                        completion([PullRequest](), error)
                    }
                }else {
                    
                    completion([PullRequest](), ErrorUtils.errorWithStatusCode(httpStatusCode: (response.response?.statusCode)!))
                }
            }
        }
    }
    
}
