//
//  ErrorUtils.swift
//  DesafioConcrete
//
//  Created by Lázaro on 30/11/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import UIKit

class ErrorUtils{
    
    class func connectionFailed() -> GitError{
        return GitError(generalError: "Connection to server failed.", httpCode: 0)
    }
    
    class func invalidResponse() -> GitError{
        return GitError(generalError: "Invalid response from the server.", httpCode: 0)
    }
    
    class func dontHaveData() -> GitError{
        return GitError(generalError: "Don't have data.", httpCode: 0)
    }
    
    
    class func errorWithStatusCode(httpStatusCode : Int) -> GitError{
    
        switch httpStatusCode {
        case 403:
            return GitError(generalError: "Forbiden(403)", httpCode: 403)
        case 404:
            return GitError(generalError: "Not Found(404)", httpCode: 404)
        case 500:
            return GitError(generalError: "Internal Server Error(500)", httpCode: 500)
        default:
            return GitError(generalError: "Unknown Error(\(httpStatusCode))", httpCode: httpStatusCode)
        }
    }
}
