//
//  PullRequestTableViewCell.swift
//  DesafioConcrete
//
//  Created by Lázaro on 30/11/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {

    static var cellIdentifier = "PullRequestCell"
    @IBOutlet weak var backgroundShape: UIView!
    @IBOutlet weak var avatarUser: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var createdAt: UILabel!
    @IBOutlet weak var pullRequestTitle: UILabel!
    @IBOutlet weak var pullRequestDescription: UILabel!
    
    var pullRequestData : PullRequest! {
        didSet {
            self.setupCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundShape.roundCorner(value: 5)
    }

    
    func setupCell() {
        self.avatarUser.sd_setImage(with: URL(string: pullRequestData.owner.avatarUrl!), placeholderImage:
            UIImage(named: "user.png"))
        self.avatarUser.roundToCircle()
        self.username.text = pullRequestData.owner.login
        self.createdAt.text = pullRequestData.createdAt
        self.pullRequestTitle.text = pullRequestData.title
        self.pullRequestDescription.text = pullRequestData.body
    }


}
