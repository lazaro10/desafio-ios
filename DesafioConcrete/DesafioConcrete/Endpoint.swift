//
//  Endpoint.swift
//  DesafioConcrete
//
//  Created by Lázaro on 30/11/16.
//  Copyright © 2016 Lázaro. All rights reserved.
//

import UIKit

enum Endpoint {
    
    case repository
    case pullRequest
    
    func description() -> String{
        switch self {
        case .repository:
            return "/search/repositories"
        case .pullRequest:
            return "/repos"
        }
    }
    
    
}
